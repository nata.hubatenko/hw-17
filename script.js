let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
const table = [];

while (true) {
	let userSubject = prompt('Write a subject');
	if (!userSubject) {
		break;
	}
	let userMark = prompt('Write your mark');

	while (isNaN(userMark) || userMark < 0 || userMark > 12) {
		userMark = prompt('Invalid input. Please enter a numerical mark between 0 and 10.');
	}
	table.push(userSubject + ':' + userMark);
}

const student = {
	firstName,
	lastName,
	table,
};
console.log(student);

const allMarks = table.map(entry => {
	return entry.split(':')[1];
});

console.log(allMarks);

for (let i = 0; i < allMarks.length; i++) {
	if (Number(allMarks[i]) > 4) {
		alert(`${firstName} переведено на наступний курс`);
		break;
	}
}

function countAv(allMarks) {
	const sum = allMarks.reduce((acc, curr) => acc + Number(curr), 0);
	const avgGrade = sum/allMarks.length;
	if (avgGrade >= 7) {
		alert(`${firstName} призначено стипендію`);
	}
	return {
		avgGrade: avgGrade
	}
}

countAv(allMarks);